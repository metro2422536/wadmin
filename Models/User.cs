using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
namespace wadmin.Models
{
    // [Authorize]
    public class AuthenUserInfo
    {
        private readonly ClaimsPrincipal _user;
        
        public AuthenUserInfo(ClaimsPrincipal user)
        {
            _user = user;
        }
        public string? EmployeeId { get => _user.Claims.FirstOrDefault(c => c.Type == "EmployeeId").Value; }
        public string? CompanyCode { get => _user.Claims.FirstOrDefault(c => c.Type == "CompanyCode").Value; }
        public string? FullName { get => _user.Claims.FirstOrDefault(c => c.Type == "given_name").Value + " " + _user.Claims.FirstOrDefault(c => c.Type == "Surname").Value; }
        public string? Email { get => _user.Claims.FirstOrDefault(c => c.Type == "email").Value; }
        public string? Role { get => _user.Claims.FirstOrDefault(c => c.Type == "role").Value; }
        public string? Detail { get => _user.Claims.FirstOrDefault(c => c.Type == "detail").Value; }
        private string[] positionslowers = {"010","011","020"};
        public bool IsManager { get => positionslowers.Contains(_user.Claims.FirstOrDefault(c => c.Type == "Position").Value.ToString())? false : true; }
        public string? SectionCode { get => _user.Claims.FirstOrDefault(c => c.Type == "SectionCode").Value ; }

        public bool IsAuthenticated {get => _user.Identity.IsAuthenticated;} 
    }
}