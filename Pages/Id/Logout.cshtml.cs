using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace wadmin.Pages.Id
{
    public class SignedOutModel : PageModel
    {
        public IActionResult OnGet()
        {
            bool? isAuthenticated = User.Identity?.IsAuthenticated;
            if (!isAuthenticated is null || isAuthenticated == true)
            {
                // Redirect to home page if the user is authenticated.
                HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                return RedirectToPage("/Index");
            }

            return Page();
        }
    }
}