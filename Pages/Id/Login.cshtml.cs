using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.IdentityModel.Tokens;

namespace wadmin.Pages.Id
{
    public class LoginModel : PageModel
    {

        public string _jwt = "";
        private readonly IConfiguration _conf;
        public LoginModel(IConfiguration conf)
        {
            _conf = conf;
        }
        public async Task<IActionResult> OnGet(string Jwt = "")
        {
            if (Jwt == "")
            {
                return Redirect(System.Web.HttpUtility.UrlDecode(_conf["AppAuthen:ServerLoginPage"] + "?AppId=" + _conf["AppAuthen:AppId"] + "&rURL=" + _conf["AppAuthen:LocalLoginPage"]));
            }
            else
            {
                _jwt = Jwt;
                if (await CheckToken(_jwt, _conf["AppAuthen:Secret"]))
                {
                    return Redirect(_conf["AppAuthen:rUrl"]);
                }
                else
                {
                    return Redirect("/Id/AccessDenied");
                }
            }
        }
        public async Task<bool> CheckToken(string token, string SecretKey)
        {
            if (token == null)
                return false;

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(SecretKey);
            try
            {
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidIssuer = "AppAuthen",
                    ValidAudience = "TYG",
                    // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                    //ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);

                var jwtToken = (JwtSecurityToken)validatedToken;
                //var userId = int.Parse(jwtToken.Claims.First(x => x.Type == "EmployeeId").Value);
                List<Claim> uClaims = jwtToken.Claims.ToList();
                uClaims.Add(new Claim(ClaimTypes.Role, uClaims.FirstOrDefault(x => x.Type == "role")?.Value ?? string.Empty));
                uClaims.Add(new Claim(ClaimTypes.GivenName, uClaims.FirstOrDefault(x => x.Type == "given_name")?.Value ?? string.Empty));
                uClaims.Add(new Claim(ClaimTypes.Email, uClaims.FirstOrDefault(x => x.Type == "email")?.Value ?? string.Empty));

                await SignIn(uClaims);
                // return user id from JWT token if validation successful
                return true;
            }
            catch
            {
                // return null if validation fails
                return false;
            }
        }

        private async Task SignIn(List<Claim> iClaim)
        {

            var claimsIdentity = new ClaimsIdentity(
            iClaim, CookieAuthenticationDefaults.AuthenticationScheme,
            ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            var authProperties = new AuthenticationProperties
            {
                IsPersistent = true
            };

            await HttpContext.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme,
            new ClaimsPrincipal(claimsIdentity), authProperties);
        }
    }
}